<?php

namespace Drupal\trinion_mail;

use Drupal\Core\File\FileSystemInterface;
use Drupal\node\Entity\Node;
use PhpImap\IncomingMailAttachment;
use PhpImap\Mailbox;

/**
 * Работа с почтовым ящиком
 */
class Mails {

  /**
   * Получение почты с созданных почтовых ящиков
   * @return void
   */
  public function getMails() {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'pochtovyy_yaschik')
      ->condition('status', 1);
    $nids = $query->accessCheck()->execute();
    foreach ($nids as $box_nid) {
      $this->getMailsFromMailBox($box_nid);
    }
  }

  /**
   * Получение почты с почтового ящика.
   */
  public function getMailsFromMailBox($box_nid) {
    $box_node = Node::load($box_nid);
    if (empty($box_node))
      return;
    $mailbox = $box_node->get('field_tm_stroka_nastroyki_imap')->getString();
    $user = $box_node->get('field_tm_email')->getString();
    $pass = $box_node->get('field_tm_parol')->getString();
    $last_uid = (int)$box_node->get('field_tm_posledniy_uid_pisma')->getString();

    $conn = new Mailbox("{{$mailbox}}INBOX", $user, $pass);
    try {
      $mailsIds = $conn->searchMailbox('SINCE ' . date('Y-m-d'));
      if(!$mailsIds) {
        return;
      }
      foreach ($mailsIds as $uid) {
        if ($uid <= $last_uid)
          continue;
        $mail = $conn->getMail($uid);
        $subject = $mail->subject;
        $from = $mail->fromAddress;
        $body = $mail->textHtml;
        if($mail->hasAttachments()) {
          $file_repository = \Drupal::service('file.repository');
          $file_system = \Drupal::service('file_system');
          /** @var IncomingMailAttachment $attach */
          $attach_placeholders = $mail->getInternalLinksPlaceholders();
          foreach ($mail->getAttachments() as $num => $attach) {
            $dir = "private://" . date('Y-m');
            $file_system->prepareDirectory($dir, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
            $file_name = $dir . "/" . $attach->name;
            $file = $file_repository->writeData($attach->getContents(), $file_name, FileSystemInterface::EXISTS_RENAME);
            $files[] = $file->id();
            $body = str_replace($attach_placeholders[$attach->contentId], $file->createFileUrl(), $body);
          }
        }

        $mail_data = [
          'type' => 'mail',
          'title' => $subject,
          'uid' => 1,
          'status' => 1,
          'field_tm_sender_email' => $from,
          'field_tm_text' => [
            'value' => $body,
            'format' => 'full_html',
          ],
          'field_tm_pochtovyy_yaschik' => $box_nid,
        ];

        $query = \Drupal::entityQuery('node')
          ->condition('type', ['kompanii', 'contact'], 'IN')
          ->condition('field_tl_email', $from);
        $res = $query->accessCheck()->execute();
        if ($res)
          $mail_data['field_tm_mail_from'] = array_values($res);
        if (!empty($files))
          $mail_data['field_tm_files'] = $files;
        $mail_node = Node::create($mail_data);
        $mail_node->save();

        $box_node->field_tm_posledniy_uid_pisma = $uid;
        $box_node->save();
      }
    } catch(PhpImap\Exceptions\ConnectionException $ex) {
    }
  }

  public function getMailAuthorUser($mail_node) {
    $query = \Drupal::entityQuery('user')
      ->condition('mail', $mail_node->get('field_tm_sender_email')->getString());
    $res = $query->accessCheck()->execute();
    $uid = reset($res);
    return $uid ? $uid : 0;
  }
}
